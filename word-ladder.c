/*
   Given two words (start and end), and a dictionary, find the length of shortest transformation sequence f
   rom start to end, such that:

   Only one letter can be changed at a time
   Each intermediate word must exist in the dictionary
   For example,

Given:
start = "hit"
end = "cog"
dict = ["hot","dot","dog","lot","log"]
As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog",
return its length 5.

Note:
Return 0 if there is no such transformation sequence.
All words have the same length.
All words contain only lowercase alphabetic characters.
 */

#include "utils.h"

bool is_visited(GraphNode* node, char* visited[], int visited_max_size) {
    int i=0;
    for(i=0; i<visited_max_size && visited[i] != NULL; i++) {
        if(strcmp(node->string_value, visited[i]) == 0)
            return true;
    }

    return false;
}

void add_to_visited(char* visited[], int visited_max_size, char* s) {
    int i=0;
    for(i=0; i<visited_max_size; i++) {
        if(visited[i] == NULL) {
            visited[i] = (char*) malloc (strlen(s));
            strcpy(visited[i], s);
            break;
        }
    }
}

int string_diff(char* s1, char* s2) {
    int i, num_diff = 0;
    for(i=0; i<strlen(s1) && num_diff < 2; i++) {
        if((char)s1[i] != (char)s2[i])
            num_diff++;
    }

    return num_diff;
}


GraphNode* get_host_neighbor(GraphNode* node, char* value, char* visited[], int visited_max_size) {
    int i=0, value_diff=0;
    GraphNode* tmpNode = NULL;

    if(is_visited(node, visited, visited_max_size))
        return NULL;

    value_diff = string_diff(node->string_value, value);

    if(value_diff == 1) 
        return node;

    add_to_visited(visited, visited_max_size, node->string_value);
    for(i=0; i<node->numNeighbors && tmpNode==NULL; i++) {
        tmpNode = get_host_neighbor(node->neighbor[i], value, visited, visited_max_size);
    }

    return tmpNode;
}

int add_to_graph(GraphNode* root, GraphNode* this) {
    int i, max_nodes = 20;
    char* visited[max_nodes];
    GraphNode* host_neighbor = NULL;
    for(i=0; i<max_nodes; i++)
        visited[i] = NULL;

    if(graph_contains_string(root, this->string_value)) {
        printf("graph already contains node %s!\n", this->string_value);
        return -1; 
    }

    //find the right node to add this to.
    host_neighbor = get_host_neighbor(root, this->string_value, visited, max_nodes);
    if(host_neighbor == NULL) {
        printf("cannot add node %s to graph\n", this->string_value);
        //cleanup
        exit(-1);
    }

    //add this to the node found
    add_as_neighbor(host_neighbor, this);

    return 0;
}

int word_ladder(char *start, char *end, char* dict[], int dict_size) {
    int i=0, retval=0;
    GraphNode* root = NULL, *newNode = NULL;
    root = new_GraphNode(-1, start);
    if(root == NULL) {
        puts("Could not get root graph node");
        exit(-1);
    }

    for(i=0; i<dict_size; i++) {
        newNode = new_GraphNode(-1, (char*) dict[i]);
        if(newNode == NULL) {
            //Cleanup
            puts("Could not get a new graph node");
            exit(-1);
        }

        retval = add_to_graph(root, newNode);
        if(retval == -1) {
            //Clean up
            puts("Could not add node to graph");
            exit(-1);
        }
    }

    newNode = new_GraphNode(-1, end);
    if(newNode == NULL) {
        puts("Could not get end graph node");
        exit(-1);
    }

    retval = add_to_graph(root, newNode);
    if(retval == -1) {
        //Clean up
        puts("Could not add node to graph");
        exit(-1);
    }

    return graph_contains_string(root, end);
}

int main(int argc, char **argv) {
    int i=0, steps=0, dict_size=5;
    char* start = "hit", *end = "cog";
    char *dict[] = {"hot", "dot", "dog", "lot", "log"};

    steps = word_ladder(start, end, dict, dict_size);
    printf("No. of steps to reach from %s to %s is %d\n", start, end, steps);

    return 0; 
}

