#include "utils.h"

int is_BST_test() {
	TreeNode *root = new_TreeNode(20);
	TreeNode *n1 = new_TreeNode(10);
	TreeNode *n2 = new_TreeNode(30);
	TreeNode *n3 = new_TreeNode(3);
	TreeNode *n4 = new_TreeNode(5);
	TreeNode *n5 = new_TreeNode(7);
	TreeNode *n6 = new_TreeNode(15);
	TreeNode *n7 = new_TreeNode(17);

	if(root == NULL || n1 == NULL || n2 == NULL || n3 == NULL || n4 == NULL || n5 == NULL || n6 == NULL || n7 == NULL) {
		puts("Could not create a tree node");
		exit(-1);
	}

	root->left = n1;
	root->right = n2;

	n1->left = n4;
	n1->right = n6;

	n4->left = n3;
	n4->right = n5;

	n6->right = n7;

	return is_BST(root);
}

int not_BST_test() {
	TreeNode *root = new_TreeNode(20);
	TreeNode *n1 = new_TreeNode(10);
	TreeNode *n2 = new_TreeNode(25);
	TreeNode *n3 = new_TreeNode(30);

	root->left = n1;
	root->right = n3;

	n1->right = n2;

	return is_BST(root);
}

int main(int argc, char **argv) {
	printf("is_BST_test: %d\n", is_BST_test());
	printf("not_BST_test: %d\n", not_BST_test());

	return 0;
}

