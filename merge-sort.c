#include "utils.h"

void merge_sort_helper(int *array, int start, int end) {
	if(start >= end)
		return;
	
	int i,mid, tmpsize = end-start+1, i1, i2;
	int tmp[tmpsize];

	mid = start + (end-start)/2;
	merge_sort_helper(array, start, mid);
	merge_sort_helper(array, mid+1, end);

	i1 = start;
	i2 = mid+1;
	i = 0;

	while(i1 <= mid && i2 <= end) {
		if(array[i1] < array[i2])
			tmp[i++] = array[i1++];
		else
			tmp[i++] = array[i2++];
	}

	while(i1 <= mid)
		tmp[i++] = array[i1++];
	while(i2 <= end)
		tmp[i++] = array[i2++];

	for(i=0; i<tmpsize; i++)
		array[start+i] = tmp[i];
}

void merge_sort(int *array, int size) {
	merge_sort_helper(array, 0, size-1);
}

int main(int argc, char **argv) {
	int size;
	puts("Input array size");
	scanf("%d", &size);
	int *array = get_array(size);
	if(NULL == array) {
		puts("Could not get an array");
		exit(-1);
	}

	print_array(array, size);
	merge_sort(array, size);
	print_array(array, size);
}