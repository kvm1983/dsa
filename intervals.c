/*
Given intervals such as the ones below,

1 ------ 6
  2--3
       4--7  8--10


find if a given interval can be contained in the given intervals completely.     
*/

#include "utils.h"

typedef struct __Interval {
	unsigned int start;
	unsigned int end;
} Interval;

void sort_intervals(Interval intervals[], int num_intervals) {
	//bubble sort
	int i;
	Interval tmp;
	bool done = false;
	while(!done) {
		done = true;
		for(i=1; i<num_intervals; i++) {
			if(intervals[i].start < intervals[i-1].start) {
				done = false;
				tmp = intervals[i];
				intervals[i] = intervals[i-1];
				intervals[i-1] = tmp;
			}
		}
	}
}

int is_contained(Interval input[], int num_intervals, Interval target) {
	int i, index, cur_end;

	//1. Sort input intervals
	sort_intervals(input, num_intervals);

	//2. Parse em and create new intervals
	Interval new_intervals[num_intervals];
	index = 0;

	new_intervals[index].start = input[0].start;
	cur_end = input[0].end;

	for(i=1; i<num_intervals; i++) {
		if(input[i].start >= cur_end) {
			//end this and start new interval;
			new_intervals[index++].end = cur_end;
			new_intervals[index].start = input[i].start;
		}

		if(input[i].end > cur_end)
			cur_end = input[i].end;
	}

	new_intervals[index++].end = cur_end;

	//3. Now see if the target can be contained
	for(i=0; i<index; i++) {
		if(target.start > new_intervals[i].start) {
			if(target.end < new_intervals[i].end)
				return 1;
		}
	}

	return 0;
}

int main(int argc, char **argv) {
	int i;
	unsigned int num_intervals = 4;

	Interval intervals[4];
	Interval target_interval_1 = {2,5};
	Interval target_interval_2 = {7,10};
	
	intervals[0].start = 8;
	intervals[0].end = 10;

	intervals[1].start = 2;
	intervals[1].end = 3;

	intervals[2].start = 1;
	intervals[2].end = 6;

	intervals[3].start = 4;
	intervals[3].end = 7;

	printf("Is 1 contained? %d\n", is_contained(intervals, num_intervals, target_interval_1));
	printf("Is 2 contained? %d\n", is_contained(intervals, num_intervals, target_interval_2));
	return 0;
}