#include <stdio.h>
#include <stdlib.h>

#include "tree_utils.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

TreeNode* new_TreeNode(int value) {
	TreeNode* newNode = (TreeNode*) malloc (sizeof(TreeNode));
	if(newNode == NULL)
		return NULL;

	newNode->value = value;
	newNode->left = NULL;
	newNode->right = NULL;

	return newNode;
}

static int in_order(TreeNode *node, int *prev) {	
	int next;

	if(node->left != NULL) {
		next = in_order(node->left, prev);
		if(next == -1)
			return -1;
		if (next < *prev) 
			return -1;
		*prev = next;
		printf("%d ", *prev);
	}

	next = node->value;
	if (next < *prev)
		return -1;

	*prev = node->value;
	printf("%d ", *prev);

	if(node->right != NULL) {
		next = in_order(node->right, prev);
		if(next == -1)
			return -1;
		if (next < *prev) 
			return -1;
		*prev = next;
		printf("%d ", *prev);
	}

	return next;
}

int is_BST(TreeNode* root) {
	int prev = -1;

	in_order(root, &prev);
	return 0;
}

int is_balanced(TreeNode *node) {
	int dl, dr;
	if(node == NULL)
		return 0;

	dl = is_balanced(node->left);
	dr = is_balanced(node->right);

	if(dl == -1 || dr == -1)
		return -1;

	if(abs(dl-dr) > 1)
		return -1;

	return (MAX(dl, dr) + 1);
}
