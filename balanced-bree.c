#include "utils.h"

void test_balanced() {
	TreeNode *root = new_TreeNode(0);
	TreeNode *n1 = new_TreeNode(0);
	TreeNode *n2 = new_TreeNode(0);
	TreeNode *n3 = new_TreeNode(0);
	TreeNode *n4 = new_TreeNode(0);
	TreeNode *n5 = new_TreeNode(0);

	root->left = n1;
	root->right = n2;

	n1->left = n3;
	n1->right = n4;

	n2->left = n5;

	printf("test_balanced returned %d\n", is_balanced(root));
}

void not_balanced() {
	TreeNode *root = new_TreeNode(0);
	TreeNode *n1 = new_TreeNode(0);
	TreeNode *n2 = new_TreeNode(0);
	TreeNode *n3 = new_TreeNode(0);
	TreeNode *n4 = new_TreeNode(0);
	TreeNode *n5 = new_TreeNode(0);

	root->left = n1;
	root->right = n2;

	n1->left = n3;
	n1->right = n4;

	n3->left = n5;

	printf("not_balanced returned %d\n", is_balanced(root));
}

int main() {
	test_balanced();
	not_balanced();
	return 0;
}