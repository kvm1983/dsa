#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "graph_utils.h"

GraphNode* new_GraphNode(int __maxNeighbors, char* string_value) {
    int maxNeighbors = __maxNeighbors;
    if(maxNeighbors <= 0)
        maxNeighbors = MAX_NEIGHBORS;

    GraphNode *node = (GraphNode*) malloc (sizeof(GraphNode));
    if(node == NULL)
        return NULL;

    node->neighbor = (GraphNode**) malloc (maxNeighbors * sizeof(GraphNode*));
    if(node->neighbor == NULL) {
        free(node);
        return NULL;
    }

    node->numNeighbors = 0;
    node->string_value = string_value;

    return node;
}

void add_as_neighbor(GraphNode* host, GraphNode* newNode) {
    host->neighbor[host->numNeighbors++] = newNode;
}

static bool is_visited(GraphNode* node, char* visited[], int visited_max_size) {
    int i=0;
    for(i=0; i<visited_max_size && visited[i] != NULL; i++) {
        if(strcmp(node->string_value, visited[i]) == 0)
            return true;
    }

    return false;
}


static void add_to_visited(char* visited[], int visited_max_size, GraphNode* node) {
    int i=0;
    for(i=0; i<visited_max_size; i++) {
        if(visited[i] == NULL) {
            visited[i] = (char*) malloc (strlen(node->string_value));
            strcpy(visited[i], node->string_value);
            break;
        }
    }
}

static bool graph_contains_string_helper(GraphNode* node, char* s, char* visited[], int visited_max_size, int* steps) {
    int i=0;
    bool found = false;

    if(is_visited(node, visited, visited_max_size))
        return false;

    (*steps)++;

    if(strcmp(node->string_value, s) == 0) {
        return true;
    }

    add_to_visited(visited, visited_max_size, node);
    
    for(i=0; i<node->numNeighbors && !found; i++) {
        found = graph_contains_string_helper(node->neighbor[i], s, visited, visited_max_size, steps);
    }

    return found;
}

int graph_contains_string(GraphNode* root, char* s) {
    int i=0, visited_max_size = 20, steps=-1;
    char* visited[visited_max_size];
    bool found = false;

    for(i=0; i<visited_max_size; i++)
        visited[i] = NULL;

    found = graph_contains_string_helper(root, s, visited, visited_max_size, &steps);

    if(found)
        return steps;

    return 0;
}

