#ifndef __INCLUDES__
#define __INCLUDES__

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>

#include "array_utils.h"
#include "rand_utils.h"
#include "graph_utils.h"
#include "tree_utils.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#endif

