#ifndef __ARRAY_UTILS__
#define __ARRAY_UTILS__

int* get_sorted_array(int size);
int* get_array(int size);
void sort_array(int* array, int size);
void print_array(int *array, int size);

#endif

