#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "rand_utils.h"

void sort_array(int* array, int size) {
    //bubble sort
    bool sorted = false;
    int i = 0, tmp;
    while(!sorted) {
        sorted = true;
        for(i=0; i<size-1; i++) {
            if(array[i] > array[i+1]) {
                tmp = array[i+1];
                array[i+1] = array[i];
                array[i] = tmp;
                sorted = false;
            }
        }
    }
}

int* get_sorted_array(int size) {
    int i = 0;
    int* array = (int*) malloc (size * sizeof(int));
    if(array == NULL)
        return NULL;

    for(i=0; i<size; i++)
        array[i] = rand_c(2*size);

    sort_array(array, size);
    return array;
}

//Returns ints between 1 and 2*size+1
int* get_array(int size) {
    int i = 0;
    int* array = (int*) malloc (size * sizeof(int));
    if(array == NULL)
        return NULL;

    for(i=0; i<size; i++)
        array[i] = rand_c(2*size)+1; //no zeros

    return array;
}

void print_array(int* array, int size) {
    int i=0;
    puts("Printing array:");
    for(i=0; i<size; i++)
        printf("%d ", array[i]);
    puts("\n----");
}

