#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool seed_set = false;

int rand_c(int maxval) {
        if(maxval < 1) maxval = 100;

        if(!seed_set) {
                srand(time(NULL));
                seed_set = true;
        }
        
        return rand() % maxval;
}

/*
int main() {
        printf("%d %d\n", rand_c(10), rand_c(100));
        return 0;
}
*/

