#ifndef __TREE_UTILS__
#define __TREE_UTILS__

typedef struct __TreeNode {
	int value;
	struct __TreeNode *left;
	struct __TreeNode *right;
} TreeNode;

TreeNode* new_TreeNode(int value);
int is_BST(TreeNode* root);
int is_balanced(TreeNode *node);

#endif
