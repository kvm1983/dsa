 /*
 I have an array stock_prices_yesterday where:

    The indices are the time, as a number of minutes past trade opening time, which was 9:30am local time.
    The values are the price of Apple stock at that time, in dollars.

For example, the stock cost $500 at 10:30am, so stock_prices_yesterday[60] = 500.

Write an efficient algorithm for computing the best profit I could have made from 
1 purchase and 1 sale of 1 Apple stock yesterday. 
For this problem, we won't allow "shorting"—you must buy before you sell. 
*/

#include "utils.h"

void best_profit(int* stock_prices_yesterday, int size) {
	int cur_min, cur_min_index, cur_max, cur_max_index, cur_profit;
	int best_min_index, best_max_index, best_profit;
	int i;

	cur_min = stock_prices_yesterday[0];
	cur_max = stock_prices_yesterday[0];
	cur_min_index = 0;
	cur_max_index = 0;
	cur_profit = 0;
	best_min_index = 0;
	best_max_index = 0;
	best_profit = 0;

	for(i=1; i<size; i++) {
		if(stock_prices_yesterday[i] <= cur_min) {
			cur_min = stock_prices_yesterday[i];			
			cur_max = stock_prices_yesterday[i];
			cur_min_index = i;
			cur_max_index = i;
		}
		else {
			cur_max = stock_prices_yesterday[i];
			cur_max_index = i;
			cur_profit = cur_max-cur_min;
			if(cur_profit > best_profit) {
				best_profit = cur_profit;				
				best_min_index = cur_min_index;
				best_max_index = cur_max_index;
			}
		}
	}

	printf("Best profit: %d, buy at %d, sell at %d\n", best_profit, stock_prices_yesterday[best_min_index], 
		stock_prices_yesterday[best_max_index]);
}

int main() {
	int size;
	puts("Enter array size");
	scanf("%d", &size);

	int* stock_prices_yesterday = get_array(size);
	print_array(stock_prices_yesterday, size);
	best_profit(stock_prices_yesterday, size);
	return 0;
}