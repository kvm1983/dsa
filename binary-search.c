#include "utils.h"

static int binary_search_helper(int* array, int start, int end, int element) {
    if(end < start)
        return -1;

    int mid = start + (end-start)/2;

    if(array[mid] == element)
        return mid;
    else if (element < array[mid])
        return binary_search_helper(array, start, mid-1, element);
    else 
        return binary_search_helper(array, mid+1, end, element);
}

int binary_search(int *array, int size, int element) {
    return binary_search_helper(array, 0, size-1, element);
}

int main() {
    int arr_size = 20, index = -1;
    int search_element = 23;

    int *sorted_array = get_sorted_array(arr_size);
    if(sorted_array == NULL) {
        puts("Could not get array");
        exit(-1);
    }

    index = binary_search(sorted_array, arr_size, search_element);

    if(index != -1)
        printf("element found at position %d\n", index);
    else
        puts("element not found");

    print_array(sorted_array, arr_size);
    free(sorted_array);
    return 0;
}

