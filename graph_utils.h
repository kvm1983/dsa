#ifndef __GRAPH_UTILS__
#define __GRAPH_UTILS__

#define MAX_NEIGHBORS 20;

typedef struct __GraphNode {
    char* string_value;
    struct __GraphNode **neighbor;
    int numNeighbors;
} GraphNode;

GraphNode* new_GraphNode(int maxNeighbors, char* string_value);
int graph_contains_string(GraphNode* root, char* value);
void add_as_neighbor(GraphNode* host, GraphNode* newNode);

#endif

